My Ray Caster:    

Basic implementation of raycasting.    

Added Features:
* Minimap with player position
* Sky
* Move speed boost (slowed rotation speed for navigation purposes)
* Strong map parsing for user input capacity.
* Generated spawn position

![raycaster in action](img1.png)

![raycaster in action](img2.png)