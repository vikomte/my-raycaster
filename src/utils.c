/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 14:34:27 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/28 15:46:58 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"
#include <stdio.h>

int		mexit(t_data *dt)
{
	if (dt->mlx_ptr && dt->img_ptr)
		mlx_destroy_image(dt->mlx_ptr, dt->img_ptr);
	if (dt->mlx_ptr && dt->win_ptr)
		mlx_destroy_window(dt->mlx_ptr, dt->win_ptr);
	map_free(dt);
	exit(1);
}

void	map_free(t_data *dt)
{
	int		i;

	i = -1;
	while (dt->map[++i])
		free(dt->map[i]);
	free(dt->map[i]);
	free(dt->map);
}

int		merror(t_data *dt, char e, int l)
{
	(void)dt;
	if (e == 'm' || e == 's' || e == 't')
		ft_putstr("Unauthorized map format:\n");
	if (e == 't')
		ft_putstr("Map must be at least a 3x3 rectangle...\n");
	if (e == 'f')
		ft_putstr("Error loading map file...\n");
	if (e == 'm')
	{
		ft_putstr("Map is not a rectangle: line ");
		ft_putnbr(l + 1);
		ft_putstr(".\n");
	}
	if (e == 's')
		ft_putstr("Unable to raycast in unclose map.\n");
	exit(-1);
}
