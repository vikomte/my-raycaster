/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:58:40 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/25 14:00:15 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

t_color		color_new(int a, int b, int c)
{
	t_color co;

	co.r = a;
	co.g = b;
	co.b = c;
	return (co);
}

t_color		color_mult(t_color a, float coef)
{
	a.r *= coef;
	a.g *= coef;
	a.b *= coef;
	return (a);
}

t_color		color_wall(t_data *dt, t_color wall)
{
	if (dt->ray.dir.x > 0 && !dt->ray.side)
		wall = color_mult(wall, 0.3);
	else if (dt->ray.dir.x < 0 && !dt->ray.side)
		wall = color_mult(wall, 0.8);
	else if (dt->ray.dir.y > 0 && dt->ray.side)
		wall = color_mult(wall, 1.3);
	else if (dt->ray.dir.y < 0 && dt->ray.side)
		wall = color_mult(wall, 1.8);
	return (wall);
}
