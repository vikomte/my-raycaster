/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:48:15 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/28 15:41:11 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

void	map_get_data(t_data *dt)
{
	int		fd;
	char	*line;
	char	**vals;

	dt->map_sy = 0;
	dt->map_sx = 0;
	if (dt->map_file && (fd = open(dt->map_file, O_RDONLY)) >= 0)
	{
		while (get_next_line(fd, &line) && (++dt->map_sy))
		{
			if (dt->map_sy == 1)
			{
				vals = ft_strsplit(line, ' ');
				while (vals[++(dt->map_sx)])
					;
				tab_free(vals);
			}
			free(line);
		}
		line ? free(line) : 0;
		close(fd);
	}
	else
		merror(dt, 'f', 0);
}

void	map_push_line(t_data *dt, char **vals, int l)
{
	int	i;

	i = -1;
	dt->map[l] = malloc(sizeof(int) * dt->map_sx);
	while (++i < dt->map_sx && vals[i])
		dt->map[l][i] = ft_atoi(vals[i]) > 0 ? 1 : 0;
}

void	map_load(t_data *dt)
{
	int		fd;
	char	*line;
	char	**vals;
	int		v;
	int		l;

	map_get_data(dt);
	v = -1;
	if (dt->map_file && (fd = open(dt->map_file, O_RDONLY)) >= 0)
	{
		l = -1;
		if (!(dt->map = malloc(sizeof(int*) * (dt->map_sy + 1))))
			return ;
		while (get_next_line(fd, &line) && (++l) >= 0)
		{
			vals = ft_strsplit(line, ' ');
			v >= 0 && tab_size(vals) != v ? merror(dt, 'm', l) : 0;
			v == -1 ? v = tab_size(vals) : 0;
			map_push_line(dt, vals, l);
			tab_free(vals);
			free(line);
		}
		line ? free(line) : 0;
		dt->map[l + 1] = NULL;
		close(fd);
		l < 2 || v < 3 ? merror(dt, 't', 0) : 0;
	}
}

int		map_verif(t_data *dt)
{
	int a;

	a = -1;
	while (++a < dt->map_sy)
		if (dt->map[a][0] != 1 || dt->map[a][dt->map_sx - 1] != 1)
			return (0);
	a = -1;
	while (++a < dt->map_sx)
		if (dt->map[0][a] != 1 || dt->map[dt->map_sy - 1][a] != 1)
			return (0);
	return (1);
}

void	map_print(t_data *dt)
{
	int	a;
	int	b;

	a = -1;
	if (!dt->map)
		return ;
	while (++a < dt->map_sy && (b = -1) < 0)
	{
		while (++b < dt->map_sx)
		{
			ft_putnbr(dt->map[a][b]);
			ft_putchar(' ');
		}
		ft_putchar('\n');
	}
}
