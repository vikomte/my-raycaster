/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 14:03:31 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/28 15:35:43 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

int	refresh(t_data *dt)
{
	move(dt);
	mexpose(dt);
	return (1);
}

int	mexpose(t_data *dt)
{
	raycast(dt);
	mlx_clear_window(dt->mlx_ptr, dt->win_ptr);
	mlx_put_image_to_window(dt->mlx_ptr, dt->win_ptr, dt->img_ptr, 0, 0);
	return (0);
}

int	key_pre(int k, t_data *dt)
{
	t_color c;

	c = color_new(123, 123, 123);
	k == 18 ? dt->world.wall_color = color_new(251, 140, 144) : c;
	k == 19 ? dt->world.wall_color = color_new(151, 140, 244) : c;
	k == 20 ? dt->world.wall_color = color_new(114, 204, 199) : c;
	k == 21 ? dt->world.wall_color = color_new(133, 106, 227) : c;
	if (k == 257)
		(dt->world.move_speed += .24) ? (dt->world.rot_speed -= .025) : 0;
	k == 126 ? dt->world.dec += 10 : 0;
	k == 125 ? dt->world.dec -= 10 : 0;
	(k == 12 || k == 123) && !dt->eve.rl ? dt->eve.rl = 1 : 0;
	(k == 14 || k == 124) && !dt->eve.rr ? dt->eve.rr = 1 : 0;
	(k == 13 || k == 126) && !dt->eve.mf ? dt->eve.mf = 1 : 0;
	(k == 1 || k == 125) && !dt->eve.mb ? dt->eve.mb = 1 : 0;
	k == 53 ? mexit(dt) : 0;
	return (0);
}

int	key_rel(int k, t_data *dt)
{
	(k == 12 || k == 123) && dt->eve.rl ? dt->eve.rl = 0 : 0;
	(k == 14 || k == 124) && dt->eve.rr ? dt->eve.rr = 0 : 0;
	(k == 13 || k == 126) && dt->eve.mf ? dt->eve.mf = 0 : 0;
	(k == 1 || k == 125) && dt->eve.mb ? dt->eve.mb = 0 : 0;
	if (k == 257)
		(dt->world.move_speed -= .24) ? (dt->world.rot_speed += .025) : 0;
	return (1);
}
