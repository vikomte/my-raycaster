/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:48:15 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/25 14:38:21 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

void	ray_draw(t_data *dt, int start, int end)
{
	short	color;
	t_color	wall;

	color = 210;
	color -= dt->ray.pw_dist * 2;
	dt->ray.side == 1 ? color /= 2 : 0;
	draw_vl_sky(dt, vec_i(dt->ray.x, 0), start, dt->world.sky_color);
	wall = color_wall(dt, dt->world.wall_color);
	draw_vl(dt, vec_i(dt->ray.x, start), end, wall);
	draw_vl(dt, vec_i(dt->ray.x, end), dt->height, dt->world.floor_color);
}

void	draw_vl(t_data *dt, t_vec_i a, int ln, t_color c)
{
	int e;

	e = a.y;
	while (e < ln)
	{
		put_ipxl(dt, c, a.x, e);
		e++;
	}
}

void	draw_vl_sky(t_data *dt, t_vec_i a, int ln, t_color c)
{
	int		e;
	float	ce;

	e = a.y;
	ce = 0.12;
	while (e < ln)
	{
		put_ipxl(dt, color_new(c.r + (e * ce * 3),
			c.g + (e * ce * 1.5), c.b + (e * ce)), a.x, e);
		e++;
	}
}
