/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 14:00:43 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/28 15:11:43 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

void	raycast(t_data *dt)
{
	dt->ray.x = -1;
	while (++(dt->ray.x) < dt->width)
		ray(dt);
	minimap_draw(dt);
}

void	core_init(t_data *dt)
{
	map_load(dt);
	if (!map_verif(dt))
		merror(dt, 's', 0);
	free(dt->map_file);
	dt->player.pos = get_spos(dt);
	dt->width = 1080;
	dt->height = 720;
	dt->mmap.size = dt->height / 4;
	dt->mlx_ptr = mlx_init();
	dt->win_ptr = mlx_new_window(dt->mlx_ptr, dt->width, dt->height,
	"wolf3d");
	dt->img_ptr = mlx_new_image(dt->mlx_ptr, dt->width, dt->height);
	dt->img = mlx_get_data_addr(dt->img_ptr, &dt->bpp, &dt->sl, &dt->endian);
	dt->mbp = dt->bpp / 8;
}

void	mmap_init(t_data *dt)
{
	dt->mmap.size_col = dt->mmap.size / dt->map_sx;
	dt->mmap.size_row = dt->mmap.size / dt->map_sy;
	dt->mmap.start = vec_i(0, dt->height - (dt->mmap.size_row * dt->map_sy));
	if (!dt->mmap.size_row)
		ft_putendl("Map too big to fit the minimap algorithm...");
}

void	world_init(t_data *dt, char **av)
{
	dt->eve.rire = 1;
	dt->eve.mf = 0;
	dt->eve.mb = 0;
	dt->eve.rr = 0;
	dt->eve.rl = 0;
	dt->player.dir.x = -1;
	dt->player.dir.y = 0;
	dt->player.plane.x = 0;
	dt->player.plane.y = 0.66;
	dt->world.move_speed = .12;
	dt->world.rot_speed = .05;
	dt->world.dec = 0.98;
	dt->world.wall_color = color_new(251, 140, 144);
	dt->world.floor_color = color_new(110, 110, 110);
	dt->world.sky_color = color_new(100, 87, 135);
	dt->world.bg_color = dt->world.sky_color;
	dt->map_file = av[1] ? ft_strdup(av[1]) :
	ft_strdup("maps/default.w3d");
}

int		main(int ac, char **av)
{
	t_data	dt;

	(void)ac;
	world_init(&dt, av);
	core_init(&dt);
	mmap_init(&dt);
	mlx_hook(dt.win_ptr, 17, (1L << 17), mexit, &dt);
	mlx_hook(dt.win_ptr, 2, (1L << 0), key_pre, &dt);
	mlx_hook(dt.win_ptr, 3, (1L << 1), key_rel, &dt);
	mlx_expose_hook(dt.win_ptr, mexpose, &dt);
	mlx_loop_hook(dt.mlx_ptr, refresh, &dt);
	mlx_loop(dt.mlx_ptr);
	return (1);
}
