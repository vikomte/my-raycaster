/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:48:15 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/25 15:02:30 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

void	ray_eval(t_data *dt)
{
	if (dt->ray.dir.x < 0)
	{
		dt->ray.step.x = -1;
		dt->ray.sdist.x = (dt->ray.pos.x - dt->ray.map.x) * dt->ray.dta.x;
	}
	if (dt->ray.dir.y < 0)
	{
		dt->ray.step.y = -1;
		dt->ray.sdist.y = (dt->ray.pos.y - dt->ray.map.y) * dt->ray.dta.y;
	}
	if (dt->ray.dir.x >= 0)
	{
		dt->ray.step.x = 1;
		dt->ray.sdist.x = (dt->ray.map.x + 1.0 - dt->ray.pos.x) * dt->ray.dta.x;
	}
	if (dt->ray.dir.y >= 0)
	{
		dt->ray.step.y = 1;
		dt->ray.sdist.y = (dt->ray.map.y + 1.0 - dt->ray.pos.y) * dt->ray.dta.y;
	}
}

void	ray_dda(t_data *dt)
{
	while (!(dt->ray.hit))
	{
		if (dt->ray.sdist.x < dt->ray.sdist.y)
		{
			dt->ray.sdist.x += dt->ray.dta.x;
			dt->ray.map.x += dt->ray.step.x;
			dt->ray.side = 0;
		}
		else
		{
			dt->ray.sdist.y += dt->ray.dta.y;
			dt->ray.map.y += dt->ray.step.y;
			dt->ray.side = 1;
		}
		dt->map[dt->ray.map.y][dt->ray.map.x] > 0 ?
		dt->ray.hit = 1 : 0;
	}
}

void	ray_dist(t_data *dt)
{
	int		start;
	int		end;
	int		lh;
	int		h2;

	h2 = dt->height / 2;
	if (!(dt->ray.side))
		dt->ray.pw_dist = ABS(((dt->ray.map.x - dt->ray.pos.x +
		(1 - dt->ray.step.x) / 2) / dt->ray.dir.x));
	else
		dt->ray.pw_dist = ABS(((dt->ray.map.y - dt->ray.pos.y +
		(1 - dt->ray.step.y) / 2) / dt->ray.dir.y));
	lh = ABS((int)(dt->height / dt->ray.pw_dist));
	start = (int)(-lh / 2 + h2);
	end = (int)(lh / 2 + h2);
	start < 0 ? start = 0 : 0;
	end >= dt->height ? end = dt->height - 1 : 0;
	ray_draw(dt, start, end + 1);
}

void	ray(t_data *dt)
{
	float cam;

	cam = (2 * (dt->ray.x) / (float)dt->width) - 1;
	dt->ray.hit = 0;
	dt->ray.pos = dt->player.pos;
	dt->ray.dir.x = dt->player.dir.x + dt->player.plane.x * cam;
	dt->ray.dir.y = dt->player.dir.y + dt->player.plane.y * cam;
	dt->ray.map = vec_i(dt->ray.pos.x, dt->ray.pos.y);
	dt->ray.dta.x = sqrt(1 + (dt->ray.dir.y * dt->ray.dir.y) /
	(dt->ray.dir.x * dt->ray.dir.x));
	dt->ray.dta.y = sqrt(1 + (dt->ray.dir.x * dt->ray.dir.x) /
	(dt->ray.dir.y * dt->ray.dir.y));
	ray_eval(dt);
	ray_dda(dt);
	ray_dist(dt);
}
