/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:48:15 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/25 14:38:21 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

void	put_ipxl(t_data *data, t_color color, int x, int y)
{
	int	r;

	color.r += 0;
	r = (x * data->mbp) + (y * data->sl);
	if ((x < data->width && y < data->height && x >= 0 && y >= 0))
	{
		(data->img)[r++] = color.b;
		(data->img)[r++] = color.g;
		(data->img)[r] = color.r;
	}
}

void	put_mipxl(t_data *data, t_color color, int x, int y)
{
	int	r;

	color.r += 0;
	r = (x * data->mbp) + (y * data->sl);
	if ((x < data->width && y < data->height && x >= 0 && y >= 0))
	{
		(data->img)[r++] += (!data->endian ? color.b : color.r) / 2;
		(data->img)[r++] += (!data->endian ? color.g : color.b) / 2;
		(data->img)[r] += (!data->endian ? color.r : color.g) / 2;
	}
}

void	fill_bg(t_data *dt)
{
	int a;
	int b;

	a = 0;
	while (a <= dt->width)
	{
		b = 0;
		while (b <= dt->height)
		{
			put_ipxl(dt, dt->world.bg_color, a, b);
			b++;
		}
		a++;
	}
}
