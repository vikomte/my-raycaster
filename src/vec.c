/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:48:15 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/25 15:10:42 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

t_vec	vec(float x, float y)
{
	t_vec v;

	v.x = x;
	v.y = y;
	return (v);
}

t_vec	vec_add(t_vec a, t_vec b)
{
	t_vec v;

	v.x = a.x + b.x;
	v.y = a.y + b.y;
	return (v);
}

t_vec	vec_sub(t_vec a, t_vec b)
{
	t_vec v;

	v.x = a.x - b.x;
	v.y = a.y - b.y;
	return (v);
}

t_vec	vec_mul(t_vec a, t_vec b)
{
	t_vec v;

	v.x = a.x * b.x;
	v.y = a.y * b.y;
	return (v);
}

t_vec_i	vec_i(float x, float y)
{
	t_vec_i v;

	v.x = (int)x;
	v.y = (int)y;
	return (v);
}
