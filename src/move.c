/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:48:15 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/25 15:01:30 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

void	rotate_cam(t_data *dt, char side)
{
	float	dx_old;
	float	coef;
	float	planex_old;

	coef = side == 'R' ? -1 : 1;
	dx_old = dt->player.dir.x;
	planex_old = dt->player.plane.x;
	dt->player.dir.x = dt->player.dir.x *
	cos(coef * dt->world.rot_speed) - dt->player.dir.y *
	sin(coef * dt->world.rot_speed);
	dt->player.dir.y = dx_old *
	sin(coef * dt->world.rot_speed) + dt->player.dir.y *
	cos(coef * dt->world.rot_speed);
	dt->player.plane.x = dt->player.plane.x *
	cos(coef * dt->world.rot_speed) - dt->player.plane.y *
	sin(coef * dt->world.rot_speed);
	dt->player.plane.y = planex_old *
	sin(coef * dt->world.rot_speed) + dt->player.plane.y *
	cos(coef * dt->world.rot_speed);
}

void	move(t_data *dt)
{
	if (dt->eve.mf)
		move_to(dt, 'F');
	if (dt->eve.mb)
		move_to(dt, 'B');
	if (dt->eve.rr)
		rotate_cam(dt, 'R');
	if (dt->eve.rl)
		rotate_cam(dt, 'L');
}

void	move_back(t_data *dt, t_vec npos)
{
	npos.x -= dt->player.dir.x * dt->world.move_speed;
	npos.y -= dt->player.dir.y * dt->world.move_speed;
	if (npos.y < (float)dt->map_sy &&
	npos.x < (float)dt->map_sx &&
	!(dt->map[(int)npos.y][(int)npos.x]))
		dt->player.pos = npos;
}

void	move_to(t_data *dt, char to)
{
	t_perso	p;
	t_world	w;
	t_vec	npos;

	p = dt->player;
	w = dt->world;
	npos = p.pos;
	if (to == 'F')
	{
		npos.x += p.dir.x * w.move_speed;
		npos.y += p.dir. y * w.move_speed;
		if (npos.y < (float)dt->map_sy &&
		npos.x < (float)dt->map_sx &&
		!(dt->map[(int)npos.y][(int)npos.x]))
			dt->player.pos = npos;
	}
	else if (to == 'B')
		move_back(dt, npos);
}
