/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minimap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:48:15 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/28 15:46:23 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_wolf.h"

void   tab_free(char **tab)
{
  int   i;

  i = -1;
  while (tab[++i])
		free(tab[i]);
  free(tab[i]);
	free(tab);
}

int		tab_size(char **tab)
{
	int	l;

	l = 0;
	while (tab && tab[l])
		++l;
	return (l);
}

t_vec	get_spos(t_data *dt)
{
	t_vec	s;

	s = vec(dt->map_sx + 0, dt->map_sy + 0);
	while (--s.y >= 0 && (s.x = dt->map_sx + 0) > 0)
		while (--s.x >= 0)
			if (!dt->map[(int)s.y][(int)s.x])
				return (vec(s.x + 0.1, s.y + 0.1));
	ft_putendl("Unable to find space to raycast..");
	exit(-1);
	return (s);
}

void	draw_mblock(t_data *dt, t_vec_i pos, int val)
{
	t_vec_i	s;
	t_color	c;
	int		i[2];

	s = vec_i(pos.x * dt->mmap.size_col,
	dt->mmap.start.y + pos.y * dt->mmap.size_row);
	i[0] = -1;
	while ((++i[0]) < dt->mmap.size_row && (i[1] = -1) < 0)
		while ((++i[1]) < dt->mmap.size_col)
		{
			c = val ? color_new(0, 128, 255) : color_new(0, 51, 25);
			c = val == 42 ? color_new(255, 0, 128) : c;
			put_mipxl(dt, c, i[1] + s.x, i[0] + s.y);
		}
}

void	minimap_draw(t_data *dt)
{
	t_vec_i	c;
	t_vec_i	pos;
	int		val;

	if (dt->mmap.size_row)
	{
		c = vec_i(-dt->mmap.size_row, dt->height - dt->mmap.size);
		pos = vec_i(-1, -1);
		while (++pos.y < dt->map_sy && (pos.x = -1) < 0)
			while (++pos.x < dt->map_sx)
			{
				val = dt->map[pos.y][pos.x] ? 1 : 0;
				(pos.x == (int)dt->player.pos.x &&
				pos.y == (int)dt->player.pos.y) ?
				val = 42 : 0;
				draw_mblock(dt, pos, val);
			}
	}
}
