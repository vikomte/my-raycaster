/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wolf.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 13:50:57 by vde-la-s          #+#    #+#             */
/*   Updated: 2016/06/28 12:06:44 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_WOLF_H
# define FT_WOLF_H

# include "libft.h"
# include <fcntl.h>
# include <mlx.h>
# include <math.h>

typedef struct	s_color
{
	int		r;
	int		g;
	int		b;
}				t_color;

typedef struct	s_vec
{
	float		x;
	float		y;
}				t_vec;

typedef struct	s_vec_int
{
	int			x;
	int			y;
}				t_vec_i;

typedef struct	s_ray
{
	t_vec		pos;
	t_vec		dir;
	t_vec		plane;

	t_vec_i		step;
	t_vec		dta;
	t_vec		sdist;
	t_vec_i		map;

	int			x;
	int			side;
	int			hit;
	float		pw_dist;

}				t_ray;

typedef struct	s_perso
{
	t_vec			pos;
	t_vec			dir;
	t_vec			plane;

}				t_perso;

typedef struct	s_world
{
	float		dec;
	float		rot_speed;
	float		move_speed;
	t_color		bg_color;
	t_color		sky_color;
	t_color		wall_color;
	t_color		floor_color;

}				t_world;

typedef struct	s_mmap
{
	int			size;
	t_vec_i		start;
	int			size_row;
	int			size_col;
}				t_mmap;

typedef struct	s_eve
{
	int		mf;
	int		mb;
	int		rr;
	int		rl;
	float	rire;
}				t_eve;

typedef struct	s_data
{
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	char			*img;
	int				bpp;
	int				mbp;
	int				endian;
	int				sl;
	int				height;
	int				width;
	int				**map;
	int				map_sy;
	int				map_sx;
	char			*map_file;
	t_ray			ray;
	t_perso			player;
	t_world			world;
	t_eve			eve;
	t_mmap			mmap;
}				t_data;

void			draw_vl(t_data *dt, t_vec_i a, int ln, t_color c);
void			draw_vl_sky(t_data *dt, t_vec_i a, int ln, t_color c);
void			draw_vl_floor(t_data *dt, t_vec_i a, int ln, t_color c);
void			put_ipxl(t_data *data, t_color color, int x, int y);
void			put_mipxl(t_data *data, t_color color, int x, int y);
void			fill_bg(t_data *dt);
void			minimap_draw(t_data *dt);
t_color			color_new(int a, int b, int c);
t_color			color_mult(t_color a, float coef);
t_color			color_wall(t_data *dt, t_color wall);
void			map_load(t_data *dt);
void			map_fill(t_data *dt);
void			map_print(t_data *dt);
int				map_verif(t_data *dt);
void			ray_draw(t_data *dt, int start, int end);
void			ray(t_data *dt);
void			raycast(t_data *dt);
int				mouse_h(int x, int y, t_data *dt);
int				mexpose(t_data *dt);
int				refresh(t_data *dt);
int				key_pre(int k, t_data *dt);
int				key_rel(int k, t_data *dt);
int				drunk(t_data *dt);
void			move(t_data *dt);
void			move_to(t_data *dt, char to);
void			rotate_left(t_data *dt);
void			rotate_right(t_data *dt);
t_vec			vec(float x, float y);
t_vec_i			vec_i(float x, float y);
t_vec			vec_add(t_vec a, t_vec b);
t_vec			vec_sub(t_vec a, t_vec b);
t_vec			vec_mul(t_vec a, t_vec b);
void			vec_rotate(t_vec *v, float b);
int				mexit(t_data *dt);
int				merror(t_data *dt, char e, int l);
void			map_free(t_data *dt);
void			tab_free(char **tab);
int				tab_size(char **tab);
t_vec			get_spos(t_data *dt);

#endif
