/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 13:58:32 by vde-la-s          #+#    #+#             */
/*   Updated: 2015/12/01 14:15:26 by vde-la-s         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strdup(const char *s1)
{
	char	*ne;

	ne = (char*)malloc(ft_strlen((char*)s1) + 1);
	if (!ne)
		return (NULL);
	ft_strcpy(ne, s1);
	return (ne);
}
