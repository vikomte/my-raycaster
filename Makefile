# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vde-la-s <vde-la-s@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/03/15 17:03:24 by vde-la-s          #+#    #+#              #
#    Updated: 2016/03/23 14:57:38 by vde-la-s         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d
IS_LIBRARY = false

CC = gcc
CFLAGS = -Wall -Werror -Wextra

MLX_FLAGS = -L/usr/local/lib/ -I/usr/local/include -lmlx -framework OpenGL -framework AppKit

SOURCES_DIR = src
OBJECTS_DIR = obj
LIBRARIES_DIR = libs
INCLUDES_DIR = include

SOURCES_FILES = \
				color.c \
				core.c \
				draw.c \
				draw.utils.c \
				event.c \
				map.c \
				minimap.c \
				move.c \
				ray.c \
				utils.c \
				vec.c \

OBJECTS_FILES = $(SOURCES_FILES:.c=.o)

COMPILED_LIBRARIES_NAMES = libft
LIBRARIES = -lft

# # # # # # # # # # # # # # # #
#	 COMPUTED VARIABLES	  #
# # # # # # # # # # # # # # # #

SOURCES = $(addprefix $(SOURCES_DIR)/,$(SOURCES_FILES))
OBJECTS = $(addprefix $(OBJECTS_DIR)/,$(OBJECTS_FILES))
COMPILED_LIBRARIES_HEADERS = $(addprefix $(INCLUDES_DIR)/,$(addsuffix .h,$(COMPILED_LIBRARIES_NAMES)))
COMPILED_LIBRARIES_DIST = $(addprefix $(LIBRARIES_DIR)/,$(addsuffix .a,$(COMPILED_LIBRARIES_NAMES)))

# # # # # # # # #
#	 RULES	 #
# # # # # # # # # 

all: $(NAME)

$(NAME): $(COMPILED_LIBRARIES_DIST) $(OBJECTS)
ifeq ($(IS_LIBRARY),true)
	ar rc $(NAME) $(OBJECTS)
	ranlib $(NAME)
else
	$(CC) $(CFLAGS) $(MLX_FLAGS) -I$(INCLUDES_DIR) -L$(LIBRARIES_DIR) $(LIBRARIES) $(OBJECTS) -o $(NAME)
endif

$(OBJECTS_DIR)/%.o: $(SOURCES_DIR)/%.c
	@mkdir $(OBJECTS_DIR) 2> /dev/null || true
	$(CC) $(CFLAGS) -I $(INCLUDES_DIR) -c $< -o $@

$(LIBRARIES_DIR)/%.a: %
	make -C $<
	@cp $</$<.a $(LIBRARIES_DIR)
	@cp $</includes/$<.h $(INCLUDES_DIR)

clean:
	@make clean -C libft
	rm -f $(OBJECTS)
	@rm -r $(OBJECTS_DIR) 2> /dev/null || true

fclean: clean
	@make fclean -C libft
	rm -f $(NAME)

dclean:
	rm -f $(COMPILED_LIBRARIES_DIST)
	rm -f $(COMPILED_LIBRARIES_HEADERS)

re: fclean all

.PHONY: all clean fclean dclean re